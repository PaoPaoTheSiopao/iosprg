﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UpgradeMenuScript : MonoBehaviour
{
    public Text pistolLvlTxt;
    public Text sniperLvlTxt;
    public Text shotgunLvlTxt;
    public Text assaultRifleLvlTxt;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pistolLvlTxt.text = "Pistol Lvl : " + Datas.Instance.pistolLvl+" \n "+"Damage = "+ Mathf.Round(Datas.Instance.pistolMultiplier*50)+"\n"+"PassThrough = "+Datas.Instance.pistolPThLvl*1;
        sniperLvlTxt.text = "Sniper Lvl : " + Datas.Instance.sniperLvl + " \n " + "Damage = " + Mathf.Round(Datas.Instance.sniperMultiplier * 100) + "\n" + "PassThrough = " + Datas.Instance.sniperPThLvl * 5;
        shotgunLvlTxt.text = "Shotgun Lvl : " + Datas.Instance.shotgunLvl+ " \n " + "Damage = " + Mathf.Round(Datas.Instance.shotgunMultiplier * 20) + "\n" + "PassThrough = " + Datas.Instance.shotgunPThLvl * 1;
        assaultRifleLvlTxt.text = "AssaultRifle Lvl : " + Datas.Instance.assaultRifleLvl + " \n " + "Damage = " + Mathf.Round(Datas.Instance.assaultRifleMultiplier * 75) + "\n" + "PassThrough = " + Datas.Instance.assaultRiflePThLvl * 2;
    }

    public void UpgradePistol()
    {
        Datas.Instance.pistolMultiplier += 0.2f;
        Datas.Instance.pistolLvl += 1;
        if(Datas.Instance.pistolLvl%4==0)
        {
            Datas.Instance.pistolPThLvl += 1;
        }
    }
    public void UpgradeSniper()
    {
        Datas.Instance.sniperMultiplier += 0.15f;
        Datas.Instance.sniperLvl += 1;
        if (Datas.Instance.sniperLvl % 6 == 0)
        {
            Datas.Instance.sniperPThLvl += 1;
        }
    }
    public void UpgradeShotgun()
    {
        Datas.Instance.shotgunMultiplier += 0.2f;
        Datas.Instance.shotgunLvl += 1;
        if (Datas.Instance.shotgunLvl % 5 == 0)
        {
            Datas.Instance.shotgunPThLvl += 1;
        }
    }
    public void UpgradeAssaultRifle()
    {
        Datas.Instance.assaultRifleMultiplier += 0.2f;
        Datas.Instance.assaultRifleLvl += 1;
        if (Datas.Instance.assaultRifleLvl % 6 == 0)
        {
            Datas.Instance.assaultRiflePThLvl += 1;
        }
    }

    public void PlayNextLevel()
    {
        if (Datas.Instance.currentLvl != 6)
        {
            SceneManager.LoadScene("Game");
        }
        else
        {
            SceneManager.LoadScene("BossBattle");
        }
    }
}
