﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public float hp;
    private float startHealth;
    public Image healthBar;


    // Start is called before the first frame update
    void Start()
    {
        startHealth = hp;
    }

    // Update is called once per frame
    void Update()
    {
        if(this.GetComponent<EnemyController>()!=null)
        {
            EnemyUpdate();   
        }
        else
        {
            PlayerUpdate();
        }
    }
    public void TakeDamage(float damage)
    {
        hp -= damage;
        if (healthBar != null)
        {
            healthBar.fillAmount = hp / startHealth;
        }
    }

    public void EnemyUpdate()
    {
        if (hp <= 0)
        {
            GameObject j = GameObject.FindGameObjectWithTag("Player");
            PlayerScript pj = j.GetComponent<PlayerScript>();
            if (this.GetComponent<EnemyController>() != null)
            {
                
                EnemyController ctrl = GetComponent<EnemyController>();
                int rng = Random.Range(0, 100);
                if (rng >= 0 && rng <= 75)
                {
                    DropPowerUp drp = this.GetComponent<DropPowerUp>();
                    drp.SpawnPowerUp();
                }
                if(ctrl.enemName == "Cultist")
                {
                    pj.playerScore += Random.Range(75,125);
                }
                else if (ctrl.enemName == "Innsmouth")
                {
                    pj.playerScore += Random.Range(200, 300);
                }
                else if (ctrl.enemName == "Crawler")
                {
                    pj.playerScore += Random.Range(125, 175);
                }
            }
            else if(this.GetComponent<PCShoot>() != null)
            {
                pj.playerScore += Random.Range(150, 175);
            }
            Destroy(gameObject);
        }
        if(Input.GetKeyUp(KeyCode.P))
        {
            SceneManager.LoadScene("UpgradingMenu");
        }
    }
    public void PlayerUpdate()
    {
        if (hp <= 0)
        {
            Destroy(gameObject);
        }
    }
}
