﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CthulhuTentacle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            GameObject player = collision.gameObject;
            Health hpPla = player.GetComponent<Health>();
            hpPla.TakeDamage(75);
        }
    }
}
