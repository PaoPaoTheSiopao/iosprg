﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cthulhu : MonoBehaviour
{
    public Transform head;
    public Transform sideLeft;
    public Transform sideRight;
    public Transform downLeft;
    public Transform downRight;

    float showArms = 0.1f;
    float wait = 0.5f;
    float waitAftAttack = 0.5f;
    float attack = 0.8f;
    float retrack = 0.928f;

    bool isStillAttacking = false;
    bool isReadyToAttack = true;

    public float speed;
    public float attackSpeed = 10;

    public float attackSpeedD = 5;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(sideAttacking());
        StartCoroutine(downAttacking());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);
        if(sideLeft==null&&sideRight==null)
        {
            SceneManager.LoadScene("GameOver");
        }
        /*
        if (retrack > 0)
        {
            sideAttack(sideLeft, sideRight);
        }
        else
        {
            resetVariable();
        }
        if(isReadyToAttack)
        {
            int randAtk = Random.Range(0, 1);
            if(randAtk == 0)
            {
                sideAttack2(sideLeft, sideRight);
            }
        }*/

    }
    /*
    void sideAttack(Transform left,Transform right)
    {
        if (showArms >= 0)
        {
            left.transform.Translate(Vector2.right * attackSpeed * Time.deltaTime);
            right.transform.Translate(Vector2.left * attackSpeed * Time.deltaTime);
            showArms -= Time.deltaTime;
        }
        else
        {
            if(wait>=0)
            {
                wait -= Time.deltaTime;
            }
            else
            {
                if(attack>=0)
                {
                    left.transform.Translate(Vector2.right * attackSpeed * Time.deltaTime);
                    right.transform.Translate(Vector2.left * attackSpeed * Time.deltaTime);
                    attack -= Time.deltaTime;
                }
                else
                {
                    if(waitAftAttack>=0)
                    {
                        waitAftAttack -= Time.deltaTime;
                    }
                    else
                    {
                        if(retrack>=0)
                        {
                            left.transform.Translate(Vector2.left * attackSpeed * Time.deltaTime);
                            right.transform.Translate(Vector2.right * attackSpeed * Time.deltaTime);
                            retrack -= Time.deltaTime;   
                        }
                    }
                }
            }
        }
    }

    void sideAttack2(Transform left, Transform right)
    {
        isStillAttacking = true;
        left.transform.Translate(Vector2.right * attackSpeed * Time.deltaTime);
        right.transform.Translate(Vector2.left * attackSpeed * Time.deltaTime);
        attack -= Time.deltaTime;
    }
    */
    IEnumerator sideAttacking()
    {
        float attackTime = 0;
        while (attackTime < 1)
        {
            //Debug.Log("time = " + attackTime);
            if (sideLeft != null)
            {
                sideLeft.transform.Translate(Vector2.right * attackSpeed * Time.deltaTime);
            }
            if (sideRight != null)
            {
                sideRight.transform.Translate(Vector2.left * attackSpeed * Time.deltaTime);
            }
            attackTime += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(sideAttackRetrack());
    }
    IEnumerator downAttacking()
    {
        float attackTime = 0;
        while (attackTime < 2)
        {
            downLeft.transform.Translate(Vector2.up * attackSpeedD * Time.deltaTime);
            downRight.transform.Translate(Vector2.up * attackSpeedD * Time.deltaTime);
            attackTime += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(downAttackRetrack());
    }

    IEnumerator sideAttackRetrack()
    {
        float attackTime = 0;
        while (attackTime < 1)
        {
            if (sideLeft != null)
            {
                sideLeft.transform.Translate(-Vector2.right * attackSpeed * Time.deltaTime);
            }
            if (sideRight != null)
            {
                sideRight.transform.Translate(-Vector2.left * attackSpeed * Time.deltaTime);
            }
            attackTime += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(sideAttacking());
    }
    IEnumerator downAttackRetrack()
    {
        float attackTime = 0;
        while (attackTime < 2)
        {
            downLeft.transform.Translate(-Vector2.up * attackSpeedD * Time.deltaTime);
            downRight.transform.Translate(-Vector2.up * attackSpeedD * Time.deltaTime);
            attackTime += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(downAttacking());
    }

    void resetVariable()
    {
        showArms = 0.1f;
        wait = 0.5f;
        waitAftAttack = 0.5f;
        attack = 0.8f;
        retrack = 0.928f;
    }
}
