﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform player;
    public float spawnInterval;
    private float spawnIntDef;
    private Transform target;
    private float cCrawlerInterval = 7;
    private float cultistInterval = 3;
    private float pCentipedeInterval = 11;
    private float innsmouthInterval = 8;
    private float cCDefaultInt;
    private float cultistDefaultInt;
    private float pCDefaultInt;
    private float innsmouthDefaultInt;
    public List<Transform> spawnPoints;
    public List<GameObject> enemies;
    public GameObject creepyCrawler;
    public GameObject poisonCentipede;
    public GameObject cultist;
    public GameObject innsmouth;
    // Start is called before the first frame update
    void Start()
    {
        spawnIntDef = spawnInterval;
        cCDefaultInt = cCrawlerInterval;
        cultistDefaultInt = cultistInterval;
        pCDefaultInt = pCentipedeInterval;
        innsmouthDefaultInt= innsmouthInterval;
}

    // Update is called once per frame
    void Update()
    {
        //SpawnIntervalM();
        Manual();
    }
    void Manual()
    {
        int randEnemyInd = Random.Range(0, enemies.Count);
        int randSpawnPntInd = Random.Range(0, spawnPoints.Count);
        target = player;
        if (cCrawlerInterval<=0)
        {
            GameObject enem = Instantiate(creepyCrawler,
                new Vector3(spawnPoints[randSpawnPntInd].transform.position.x + Random.Range(-10, 10),
                spawnPoints[randSpawnPntInd].transform.position.y, spawnPoints[randSpawnPntInd].transform.position.z),
                transform.rotation);
            EnemyController enemRoam = enem.GetComponent<EnemyController>();
            enemRoam.setParameters(getAngle(enem, player));
            cCrawlerInterval = cCDefaultInt;
        }
        if (cultistInterval <= 0)
        {
            GameObject enem = Instantiate(cultist,
                new Vector3(spawnPoints[randSpawnPntInd].transform.position.x + Random.Range(-10, 10),
                spawnPoints[randSpawnPntInd].transform.position.y, spawnPoints[randSpawnPntInd].transform.position.z),
                transform.rotation);
            EnemyController enemRoam = enem.GetComponent<EnemyController>();
            enemRoam.setParameters(getAngle(enem, player));
            cultistInterval = cultistDefaultInt;
        }
        if (pCentipedeInterval <= 0)
        {
            GameObject enem = Instantiate(poisonCentipede,
                new Vector3(spawnPoints[randSpawnPntInd].transform.position.x + Random.Range(-10, 10),
                spawnPoints[randSpawnPntInd].transform.position.y, spawnPoints[randSpawnPntInd].transform.position.z),
                transform.rotation);
            pCentipedeInterval = pCDefaultInt;
        }
        if (innsmouthInterval <= 0)
        {
            GameObject enem = Instantiate(innsmouth,
                new Vector3(spawnPoints[randSpawnPntInd].transform.position.x + Random.Range(-10, 10),
                spawnPoints[randSpawnPntInd].transform.position.y, spawnPoints[randSpawnPntInd].transform.position.z),
                transform.rotation);
            EnemyController enemRoam = enem.GetComponent<EnemyController>();
            enemRoam.setParameters(getAngle(enem, player));
            innsmouthInterval = innsmouthDefaultInt;
        }
        cCrawlerInterval -= Time.deltaTime;
        innsmouthInterval -= Time.deltaTime;
        pCentipedeInterval -= Time.deltaTime;
        cultistInterval -= Time.deltaTime;
    }
    void SpawnIntervalM()
    {
        if (spawnInterval <= 0 && player != null)
        {
            int randEnemyInd = Random.Range(0, enemies.Count);
            int randSpawnPntInd = Random.Range(0, spawnPoints.Count);
            target = player;
            GameObject enem = Instantiate(enemies[randEnemyInd],
                new Vector3(spawnPoints[randSpawnPntInd].transform.position.x + Random.Range(-10, 10),
                spawnPoints[randSpawnPntInd].transform.position.y, spawnPoints[randSpawnPntInd].transform.position.z),
                transform.rotation);
            if (enem.GetComponent<EnemyRoaming>() != null)
            {
                EnemyController enemRoam = enem.GetComponent<EnemyController>();
                enemRoam.setParameters(getAngle(enem, player));
            }
            spawnInterval = spawnIntDef;
        }
        spawnInterval -= Time.deltaTime;
    }
    public float getAngle(GameObject enemy,Transform playe)
    {
        //Debug.Log("Getting angle");
        //Debug.Log("enemy = "+enemy.transform.position+"  Target = "+target.position);
            
        Vector3 dis = playe.transform.position - enemy.transform.position;

        float angle = Mathf.Rad2Deg * Mathf.Atan2(dis.y, dis.x);
        //Debug.Log("angle = "+angle);
        return angle;
    }
}
