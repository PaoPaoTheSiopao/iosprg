﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour
{
    public TestTouch touchRef;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(touchRef.touchPosition.x, touchRef.touchPosition.y);
        //Debug.Log("New touch detected at position " + touchRef.touchPosition);
    }
}
