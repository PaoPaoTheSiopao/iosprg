﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    public GameObject Hero;
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject enemy;
        public int size;
    }

    #region Singleton
    public static EnemyPool Instance;

    private void Awake()
    {
        Instance = this;
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in enemyPools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.enemy);

                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionary.Add(pool.tag, objectPool);
        }
    }
    #endregion
    public List<Pool> enemyPools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    void Start()
    {
        
    }

    public GameObject SpawnEnemyFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        //print("Setting " + tag + " Active");
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with Tag " + tag + " doesn't exist");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        poolDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }

}

