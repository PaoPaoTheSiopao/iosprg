﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRoaming : MonoBehaviour
{
    public float enemAngle;
    public float enemSpeed;
    public float enemDamage;
    Rigidbody2D rb;
    float smooth = 5;
    Vector2 test = new Vector2(1f, 0.5f);
    Vector2 enemyAngle;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //rb.AddForce(Vector3.right*enemSpeed*Time.deltaTime);
        enemyGetAngle();
        rb.AddForce(enemyAngle * enemSpeed * Time.deltaTime);
        Quaternion target = Quaternion.Euler(0, 0, enemAngle);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
    }
    public void setParameters(float angle)
    {
        enemAngle = angle;
    }
    void enemyGetAngle()
    {
        enemyAngle.x = Mathf.Cos(Mathf.Deg2Rad * enemAngle);
        enemyAngle.y = Mathf.Sin(Mathf.Deg2Rad * enemAngle);
    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "DestroyZone")
        {
            Destroy(gameObject);
        }
        if(collision.gameObject.tag == "Player")
        {
            Health hp = collision.GetComponent<Health>();
            hp.TakeDamage(enemDamage);
        }
    }


}
