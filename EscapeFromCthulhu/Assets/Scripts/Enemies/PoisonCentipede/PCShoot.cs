﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCShoot : MonoBehaviour
{
    private float fireRate;
    public float setFireRate;
    public float damage;

    public GameObject bulletGoo;
    public Transform gooSpawnPoint;
    private Transform player;
    private Health pcHp;
    public Spawner spawner;

    public float angle;
    public float centipedeAngle;
    // Start is called before the first frame update
    void Start()
    {
        fireRate = setFireRate;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        pcHp = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fireRate <= 0)
        {
            FireBullet();
            fireRate = setFireRate;
        }
        fireRate -= Time.deltaTime;

        transform.eulerAngles = new Vector3(0f, 0f, angle);
        LookAtPlayer();
    }

    void FireBullet()
    {
        GameObject bull = Instantiate(bulletGoo, gooSpawnPoint.transform.position, transform.rotation);
        PCBullet bulet = bull.gameObject.GetComponent<PCBullet>();
        bulet.bulletAngle = angle;
    }

    void LookAtPlayer()
    {
        Vector3 dis = player.transform.position - this.transform.position;

        angle = Mathf.Rad2Deg * Mathf.Atan2(dis.y,dis.x);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "DestroyZone")
        {
            Destroy(gameObject);    
        }
    }
}
