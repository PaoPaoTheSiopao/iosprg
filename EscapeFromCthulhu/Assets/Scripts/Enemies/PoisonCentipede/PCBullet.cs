﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCBullet : MonoBehaviour
{
    public float bulletAngle;
    public float bulletSpeed;
    public float bulletDamage;
    public int passThrough = 1;

    // Start is called before the first frame update
    void Start()
    {
        //GameObject player = GameObject.FindGameObjectWithTag("Player");
        //PlayerScript plau = player.GetComponent<PlayerScript>();
        //bulletAngle = plau.curAngle;
        //Debug.Log("Bullet Angle = " + bulletAngle);
    }

    // Update is called once per frame
    void Update()
    {

        transform.eulerAngles = new Vector3(0f, 0f, bulletAngle);
        transform.Translate(Vector2.right * bulletSpeed * Time.deltaTime);
        if (passThrough <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    /*
    public void SetBulletVariables(float bulDam,float bulSpeed,float bulAng)
    {
        bulletAngle = bulAng;
        bulletDamage = bulDam;
        bulletSpeed = bulSpeed;
    }*/
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "DestroyZone")
        {
            Destroy(gameObject);    
        }
        else if (collision.gameObject.tag == "Player")
        {
            //Debug.Log("enemy colided");
            Health colHealth = collision.GetComponent<Health>();
            colHealth.TakeDamage(bulletDamage);
            passThrough -= 1;
        }
    }
}
