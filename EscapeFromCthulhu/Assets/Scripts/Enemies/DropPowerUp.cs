﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropPowerUp : MonoBehaviour
{
    public List<GameObject> powerPickUps;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnPowerUp()
    {
        int rng = Random.Range(0, powerPickUps.Count);
        Instantiate(powerPickUps[rng], this.transform.position, this.transform.rotation);
    }
}
