﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemySet
{
    public string name;
    public List<EnemyType> set;
}
