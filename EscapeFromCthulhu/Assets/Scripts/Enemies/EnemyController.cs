﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public float enemAngle;
    public string enemName;
    public float enemSpeed;
    public float enemDamage;
    private Transform player;
    public Health enemCHp;
    Rigidbody2D rb;
    float smooth = 5;
    Vector2 test = new Vector2(1f, 0.5f);
    Vector2 enemyAngle;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        enemAngle = getAngle(this.gameObject, player);
        enemCHp = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        //rb.AddForce(Vector3.right*enemSpeed*Time.deltaTime);
        Move();
    }
    public void setParameters(float angle)
    {
        enemAngle = angle;
    }

    protected virtual void Died()
    {

    }

    public float getAngle(GameObject enemy, Transform playe)
    {
        //Debug.Log("Getting angle");
        //Debug.Log("enemy = "+enemy.transform.position+"  Target = "+target.position);

        Vector3 dis = playe.transform.position - enemy.transform.position;

        float angle = Mathf.Rad2Deg * Mathf.Atan2(dis.y, dis.x);
        //Debug.Log("angle = "+angle);
        return angle;
    }
    protected virtual void Move()
    {
        //Debug.Log("Trying to Move");
        transform.eulerAngles = new Vector3(0f, 0f, enemAngle);
        transform.Translate(Vector2.right * enemSpeed * Time.deltaTime);
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "DestroyZone")
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Player")
        {
            PlayerScript ply = collision.GetComponent<PlayerScript>();
            //Debug.Log("IsVulnerable = " + ply.isVulnerable);
            if (ply.isVulnerable)
            {
                Health hp = collision.GetComponent<Health>();
                //Debug.Log("Damage = " + enemDamage);
                hp.TakeDamage(enemDamage);
            }
        }
    }
}
