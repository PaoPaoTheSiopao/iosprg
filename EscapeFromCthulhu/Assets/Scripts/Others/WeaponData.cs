﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewWeaponStat",menuName ="WeaponsStats")]
public class WeaponData : ScriptableObject
{
    public float pistolMultiplier = 1;
    public float sniperMultiplier = 1;
    public float shotgunMultiplier = 1;
    public float assaultRifleMultiplier = 1;

}
