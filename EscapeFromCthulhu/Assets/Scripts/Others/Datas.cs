﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Datas : Singleton<Datas>
{
    //Weapons
    public float pistolMultiplier = 1;
    public float sniperMultiplier = 1;
    public float shotgunMultiplier = 1;
    public float assaultRifleMultiplier = 1;

    public int pistolLvl = 1;
    public int sniperLvl = 1;
    public int shotgunLvl = 1;
    public int assaultRifleLvl = 1;

    public int pistolPThLvl = 1;
    public int shotgunPThLvl = 1;
    public int sniperPThLvl = 1;
    public int assaultRiflePThLvl = 1;
    public int currentAvailWeapInd = 0;
    /////////////////////////////////////

    //Level Adjustmnets
    public int currentLvl = 1;
    public int currentWaves = 0;
    public float nextLvlGoal = 500;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void NextLevelFunction()
    {
        if(currentWaves<3)
        {
            currentWaves++;
        }
        if (currentAvailWeapInd < 3)
        {
            currentAvailWeapInd++;
        }
        if (currentLvl >= 6)
        {
            nextLvlGoal = 100000000000;
        }
        else
        {
            nextLvlGoal = (nextLvlGoal * 0.5f) + nextLvlGoal;
        }
    }
}
