﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveJoyStick : MonoBehaviour
{
    public Transform player;
    public Animator anim;
    public GameObject objPlayer;
    public float speed = 5.0f;
    private bool touchStart = false;

    private Vector2 pointA;
    private Vector2 pointB;

    public RectTransform circle;
    public RectTransform outerCircle;

    public Vector2 joyStickOrigin;
    private RectTransform rTransform;
    // Start is called before the first frame update
    void Start()
    {
        rTransform = GetComponent<RectTransform>();
        anim = objPlayer.GetComponent<Animator>();
    }

    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            
            if(Input.mousePosition.x<= Screen.width / 2)
            { 
            pointA = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            //Debug.Log("Mouse Pos = "+pointA);
            circle.position = pointA;
            outerCircle.position = pointA;
            anim.SetBool("IsWalking", true);
            circle.GetComponent<Image>().color = new Color(1,1,1,1);
            outerCircle.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            }
            else
            {
                return;
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (Input.mousePosition.x <= Screen.width / 2)
            {
                touchStart = true;
                pointB = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }
            else
            {
                return;
            }

        }
        else
        {
            touchStart = false;
        }
    }

    private void FixedUpdate()
    {
        if (touchStart)
        {
            Vector2 offset = pointB - pointA;
            //Debug.Log("TouchStart");
            Vector2 direction = Vector2.ClampMagnitude(offset, 20.0f);
            //Debug.Log("Clamp Direction = " + direction);
            movePlayer(direction);
            circle.transform.position = new Vector2(pointA.x + direction.x, pointA.y + direction.y );
        }
        else
        {

            anim.SetBool("IsWalking", false);
            circle.GetComponent<Image>().color = new Color(1, 1, 1, 0.0f);
            circle.position = pointA;
            outerCircle.GetComponent<Image>().color = new Color(1, 1, 1, 0.0f);
        }
    }

    void movePlayer(Vector3 direction)
    {

        //Debug.Log("Moving");
        //player.Translate(direction * speed * Time.deltaTime);
        player.transform.position += direction * speed * Time.deltaTime;
    }
}
