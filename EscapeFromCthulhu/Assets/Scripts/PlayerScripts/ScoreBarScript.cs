﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBarScript : MonoBehaviour
{
    private Text scoreText;
    private GameObject hero;
    private PlayerScript scr;
    // Start is called before the first frame update
    void Start()
    {
        scoreText = GetComponent<Text>();
        hero = GameObject.FindGameObjectWithTag("Player");
        scr = hero.GetComponent<PlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score = " + scr.playerScore;
    }
}
