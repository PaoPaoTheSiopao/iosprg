﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsScript : MonoBehaviour
{
    public float curBulAngle;
    public WeaponData wpnData;

    public string curWeapon;
    public PlayerScript player;
    public GameObject curBullet;
    public GameObject midasBullet;
    public Transform bulletSpawnPoint;
    public List<GameObject> bullets;
    public float fireRate;
    public float curBulInd;

    public float sniperFireRate;
    public float setSniperFireRate;
    public float pistolFireRate;
    public float setPistolFireRate;
    public float assaultRifleFireRate;
    public float setAssaultRifleFireRate;
    public float shotgunFireRate;
    public float setShotgunFireRate;

    public bool weaponBuff = false;

    enum WeaponType
    {
        Sniper,
        Pistol,
        AssaultRifle,
        Shotgun
    }
    enum WeaponFireRate
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerScript>();
        curBullet = bullets[0];
        setAssaultRifleFireRate = assaultRifleFireRate;
        setShotgunFireRate = shotgunFireRate;
        setPistolFireRate = pistolFireRate;
        setSniperFireRate = sniperFireRate;
    }

    // Update is called once per frame
    void Update()
    {
        if (fireRate <= 0)
        {
            if(curWeapon!=null)
            {
                curBullet = bullets[player.curBulletIndex];
                Shoot();
            }
            if (weaponBuff)
            {
                fireRate = 0.09f;
            }
            else
            {
                SetFireRate();
            }
        }
        fireRate -= Time.deltaTime;
    }
    public void UpdateFireRate()
    {
        switch (curWeapon)
        {
            case "Sniper":
                fireRate = sniperFireRate;
                break;
            case "Pistol":
                fireRate = pistolFireRate;
                break;
            case "AssaultRifle":
                fireRate = assaultRifleFireRate;
                break;
            case "Shotgun":
                fireRate = shotgunFireRate;
                break;
        }
    }
    void SetFireRate()
    {
        switch (curWeapon)
        {
            case "Sniper":
                fireRate = setSniperFireRate;
                break;
            case "Pistol":
                fireRate = setPistolFireRate;
                break;
            case "AssaultRifle":
                fireRate = setAssaultRifleFireRate;
                break;
            case "Shotgun":
                fireRate = setShotgunFireRate;
                break;
        }
    }
    void ChangeBullet()
    {
        if(curBullet.gameObject.tag == "NBullet")
        {
            curBullet = bullets[1];
        }
        else
        {
            curBullet = bullets[0];
        }
    }
    void Shoot()
    {
        //Datas.Instance.sniperMultiplier = 3;

        Bullet bull;
        float rand;
        switch (curWeapon)
        {
            case "Sniper":
                bull = Instantiate(curBullet, bulletSpawnPoint.position, transform.rotation).GetComponent<Bullet>();
                if (player.curBulletIndex == 1)
                {
                    bull.ModifyValues(30, 1000 * Datas.Instance.sniperMultiplier, 2 * Datas.Instance.sniperPThLvl);
                }
                else
                {
                    bull.ModifyValues(30, 100 * Datas.Instance.sniperMultiplier, 2 * Datas.Instance.sniperPThLvl);
                }
                bull.ModifyAngle(curBulAngle);
                break;
            case "Pistol":
                bull = Instantiate(curBullet, bulletSpawnPoint.position, transform.rotation).GetComponent<Bullet>();
                if (player.curBulletIndex == 1)
                {
                    bull.ModifyValues(10, 1000 * Datas.Instance.pistolMultiplier, 1 * Datas.Instance.sniperPThLvl);
                }
                else
                {
                    bull.ModifyValues(10, 50 * Datas.Instance.pistolMultiplier, 1 * Datas.Instance.sniperPThLvl);
                }
                Debug.Log("Pistol Angle: " + curBulAngle);
                bull.ModifyAngle(curBulAngle);
                break;
            case "AssaultRifle":
                bull = Instantiate(curBullet, bulletSpawnPoint.position, transform.rotation).GetComponent<Bullet>();
                bull.ModifyValues(20, 75 * Datas.Instance.assaultRifleMultiplier, 1 * Datas.Instance.sniperPThLvl);
                rand = Random.Range(curBulAngle - .05f, curBulAngle + .05f);
                //Debug.Log(rand)
                bull.ModifyAngle(rand);
                //Debug.Log("Cur = "+curBulAngle * Mathf.Deg2Rad);
                Debug.Log("RandBull = " + rand);
                break;
            case "Shotgun":
                for (int i = 0; i <= 5; i++)
                {
                    bull = Instantiate(curBullet, bulletSpawnPoint.position, transform.rotation).GetComponent<Bullet>();
                    bull.ModifyValues(Random.Range(15,20), 20 * Datas.Instance.shotgunMultiplier, 1 * Datas.Instance.sniperPThLvl);
                    rand = Random.Range(curBulAngle - .2f, curBulAngle + .2f);
                    bull.ModifyAngle(rand);
                    Debug.Log("Cur = " + curBulAngle);
                    Debug.Log("RandBull = " + rand);
                }
                break;
        }
    }
}
