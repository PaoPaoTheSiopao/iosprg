﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiracleMagazine : MonoBehaviour
{
    GameObject player;
    PlayerScript ply;
    WeaponsScript wpn;
    float defaultFireRate;
    public float duration;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        ply = player.GetComponent<PlayerScript>();
        wpn = player.GetComponent<WeaponsScript>();
        defaultFireRate = ply.setFireRate;
        wpn.weaponBuff = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(duration<=0)
        {
            wpn.weaponBuff = false;
            Destroy(this.gameObject);
        }
        duration -= Time.deltaTime;
    }
}
