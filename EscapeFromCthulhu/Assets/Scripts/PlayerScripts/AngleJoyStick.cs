﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AngleJoyStick : MonoBehaviour
{

    public Transform player;
    public float speed = 5.0f;
    private bool touchStart = false;
    float angle = 0;

    private Vector2 pointA;
    private Vector2 pointB;

    public RectTransform circle;
    public RectTransform outerCircle;

    public Vector2 joyStickOrigin;
    private RectTransform rTransform;

    GameObject playe;
    // Start is called before the first frame update
    void Start()
    {
        rTransform = GetComponent<RectTransform>();
        playe = GameObject.FindGameObjectWithTag("Player");
        
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {

            if (Input.mousePosition.x > Screen.width/2)
            {
                pointA = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                //Debug.Log("Mouse Pos = " + pointA);
                circle.position = pointA;
                outerCircle.position = pointA;
                circle.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                outerCircle.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            }
            else
            {
                return;
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (Input.mousePosition.x > Screen.width / 2)
            {
                touchStart = true;
                pointB = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }
            else
            {
                return;
            }

        }
        else
        {
            touchStart = false;
        }

        playe.GetComponent<PlayerScript>().curAngle = angle;
        //Debug.Log("JoyStick Angle = " + angle);

    }

    private void FixedUpdate()
    {
        if (touchStart)
        {
            Vector2 offset = pointB - pointA;
            //Debug.Log("TouchStart");
            Vector2 direction = Vector2.ClampMagnitude(offset, 20.0f);
            //Debug.Log("Clamp Direction = " + direction);
            movePlayerAngle(direction);
            circle.transform.position = new Vector2(pointA.x + direction.x, pointA.y + direction.y);
        }
        else
        {
            circle.GetComponent<Image>().color = new Color(1, 1, 1, 0.0f);
            circle.position = pointA;
            outerCircle.GetComponent<Image>().color = new Color(1, 1, 1, 0.0f);
        }
    }
    
    void movePlayerAngle(Vector2 direction)
    {
        
        //Debug.Log("Moving");
        //angle = Vector2.Angle(Vector2.right, direction);
        angle = Mathf.Atan2(direction.y-Vector2.right.y,direction.x-Vector2.right.x);
        player.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Rad2Deg*angle);
    }
}

