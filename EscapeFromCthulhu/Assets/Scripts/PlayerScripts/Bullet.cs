﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletAngle;
    public float bulletSpeed;
    public float bulletDamage;
    public int bulletPassThrough = 1;

    // Start is called before the first frame update
    void Start()
    {
        //GameObject player = GameObject.FindGameObjectWithTag("Player");
        //PlayerScript plau = player.GetComponent<PlayerScript>();
        //bulletAngle = plau.curAngle;
        //Debug.Log("Bullet Angle = " + bulletAngle);
    }

    // Update is called once per frame
    void Update()
    {

        transform.eulerAngles = new Vector3(0f, 0f, Mathf.Rad2Deg * bulletAngle);
        transform.Translate(Vector2.right * bulletSpeed * Time.deltaTime);
        if(bulletPassThrough <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void ModifyValues(float speed, float damage, int passThrough)
    {
        bulletDamage = damage;
        bulletSpeed = speed;
        bulletPassThrough = passThrough;
    }
    public void ModifyAngle(float angle)
    {
        bulletAngle = angle;
    }
    /*
    public void SetBulletVariables(float bulDam,float bulSpeed,float bulAng)
    {
        bulletAngle = bulAng;
        bulletDamage = bulDam;
        bulletSpeed = bulSpeed; 
    }*/
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "DestroyZone")
        {
            Destroy(gameObject);
        }
        else if(collision.gameObject.tag == "Enemy")
        {
            //Debug.Log("enemy colided");
            Health colHealth = collision.GetComponent<Health>();
            colHealth.TakeDamage(bulletDamage);
            print("Enemy = " + collision.gameObject.name + "took " + bulletDamage + " Damage");
            bulletPassThrough -= 1;
        }
        else if (collision.gameObject.tag == "CthulhuTentacle")
        {
            //Debug.Log("enemy colided");
            Health colHealth = collision.GetComponent<Health>();
            colHealth.TakeDamage(bulletDamage);
            print("Cthulu = " + collision.gameObject.name + "took " + bulletDamage + " Damage");
            bulletPassThrough -= 1;
        }
    }
}
