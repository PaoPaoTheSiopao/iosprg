﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{
    public float curAngle;
    private WeaponsScript wpn;
    public AngleJoyStick angle;
    public Transform bulletSpawn;
    public float fireRate;
    public float setFireRate;
    public bool midasMode = false;
    public bool isVulnerable = true;
    public GameObject bullet;
    public GameObject midasBullet;
    public string[] Weapons = {"Pistol","Sniper","Shotgun","AssaultRifle"};
    public int curWeaponInd;
    SpriteRenderer sprt;
    public int playerScore;
    public int curBulletIndex = 0;
    private int availWeapInd;

    private float playerScoreGoal;

    public Health hlt;
    // Start is called before the first frame update
    void Start()
    {
        fireRate = setFireRate;
        hlt = GetComponent<Health>();
        sprt = GetComponent<SpriteRenderer>();
        wpn = GetComponent<WeaponsScript>();
        curWeaponInd = 0;
        wpn.curWeapon = Weapons[curWeaponInd];
        availWeapInd = Datas.Instance.currentAvailWeapInd;
        playerScoreGoal = Datas.Instance.nextLvlGoal;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Health = " + hlt.hp);
        //Debug.Log("CurAngle = " + curAngle);
        //Debug.Log("isVulnerble = " + isVulnerable);
        
        if(isVulnerable)
        {
            sprt.color = new Color(sprt.color.r, sprt.color.g, sprt.color.b, 1f);
        }
        else
        {
            sprt.color = new Color(sprt.color.r, sprt.color.g, sprt.color.b, 0.5f);
        }
        if(Input.GetKeyDown(KeyCode.Q))
        {
            ChangeWeapon(-1);
            Debug.Log("clicked Q");
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            ChangeWeapon(1);
            Debug.Log("clicked E");
        }
        wpn.curBulAngle = curAngle;
        if(playerScore>=playerScoreGoal)
        {
            Datas.Instance.currentLvl++;
            Datas.Instance.NextLevelFunction();
            SceneManager.LoadScene("UpgradingMenu");
        }
    }

    void ChangeWeapon(int num)
    {
        if(curWeaponInd<=0&&num<0)
        {
            curWeaponInd = availWeapInd;
        }
        else if(curWeaponInd>= availWeapInd && num>0)
        {
            curWeaponInd = 0;
        }
        else
        {
            curWeaponInd += num;
        }
        wpn.curWeapon = Weapons[curWeaponInd];
        wpn.UpdateFireRate();
        Debug.Log("weapon ind = " + curWeaponInd);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Cthulhu")
        {
            SceneManager.LoadScene("GameOver");
        }
        if(collision.gameObject.tag == "DestroyZone")
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
