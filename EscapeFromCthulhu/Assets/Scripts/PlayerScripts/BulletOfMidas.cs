﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletOfMidas : MonoBehaviour
{
    GameObject player;
    PlayerScript ply;
    public float duration;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        ply = player.GetComponent<PlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        ply.midasMode = true;
        ply.curBulletIndex = 1;
        if (duration <= 0)
        {
            ply.midasMode = false;
            ply.curBulletIndex = 0;
            Destroy(this.gameObject);
        }
        duration -= Time.deltaTime;
    }
}
