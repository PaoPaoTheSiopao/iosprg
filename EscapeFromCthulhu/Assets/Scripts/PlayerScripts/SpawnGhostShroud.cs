﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGhostShroud : SpawnPowerUp
{
    public override void InstatiatePowerUp()
    {
        Instantiate(powerUp, this.transform.position, this.transform.rotation);
    }
}
