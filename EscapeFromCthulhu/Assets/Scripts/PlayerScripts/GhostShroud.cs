﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostShroud : MonoBehaviour
{
    GameObject player;
    PlayerScript ply;
    float defaultFireRate;
    public float duration;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        ply = player.GetComponent<PlayerScript>();
        
    }

    // Update is called once per frame
    void Update()
    {
        ply.isVulnerable = false;
        if (duration <= 0)
        {
            ply.isVulnerable = true;
            Destroy(this.gameObject);
        }
        duration -= Time.deltaTime;
    }
}
