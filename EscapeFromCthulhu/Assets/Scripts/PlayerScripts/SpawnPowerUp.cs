﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPowerUp : MonoBehaviour
{
    public GameObject powerUp;
    private void Start()
    {
    }
    void Update()
    {
        
    }
    public virtual void InstatiatePowerUp()
    {
        Debug.Log("SpawningPower Up");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            InstatiatePowerUp();
            Destroy(this.gameObject);
        }
    }
}
