﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTouch : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector2 touchPosition;
    public Transform analog;
    public Vector2 touchBegan;
    public Vector2 touchEnd;
    public Vector2 originTouch;
    private Vector2 lastPos;
    public float offset;
    float distance;
    void Start()
    {
        //touchPosition = originTouch;
    }
    
    void Update()
    {

    #if UNITY_IOS || UNITY_ANDROID
        InputTouch();
    #else
        InputMouse();
#endif
        //transform.position = new Vector3(Mathf.Clamp(),);
        //touchPosition = touchPosition.normalized;
        
        touchPosition = touchPosition.normalized * Mathf.Clamp(touchPosition.magnitude,0f,2f);

    }
    
    void InputTouch()
    {
        int touches = Input.touchCount;

        if (touches > 0)
        {
            for (int i = 0; i < touches; i++)
            {
                Touch touch = Input.GetTouch(i);

                TouchPhase phase = touch.phase;

                switch (phase)
                {
                    case TouchPhase.Began:
                        Debug.Log("New touch detected at position " + touch.position + " , index " + touch.fingerId);
                        break;
                    case TouchPhase.Moved:
                        Debug.Log("Touch index " + touch.fingerId + " has moved by " + touch.deltaPosition);
                        break;
                    case TouchPhase.Stationary:
                        Debug.Log("Touch index " + touch.fingerId + " is stationary at position " + touch.position);
                        break;
                    case TouchPhase.Ended:
                        Debug.Log("Touch index " + touch.fingerId + " ended at position " + touch.position);
                        break;
                    case TouchPhase.Canceled:
                        Debug.Log("Touch index " + touch.fingerId + " cancelled");
                        break;
                }
            }
        }
    }

    enum MouseState
    {
        Began = 0,
        Move = 1,
        Ended = 2,
        Stationary = 3,
        Null = -1
    }

    void InputMouse()
    {
        MouseState mouseState = MouseState.Null;

        if(Input.GetMouseButtonDown(0))
        {
            mouseState = MouseState.Began;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            mouseState = MouseState.Ended;
        }
        else if(Input.GetMouseButton(0))
        {
            mouseState = MouseState.Move;
            touchEnd = new Vector2(touchPosition.x, touchPosition.y);
            
            if(CalculateDistance(touchBegan, touchEnd)> offset)
            {
                Debug.Log("Need to Move");

                touchEnd = new Vector2(0, 0);
            }
            else
            {
                Debug.Log("Not Need to Move");

                touchEnd = new Vector2(0, 0);
            }
        }

        switch(mouseState)
        {
            case MouseState.Began:
                //Debug.Log("New touch detected at position " + Camera.main.ScreenToWorldPoint(Input.mousePosition));
                touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                touchBegan = new Vector2(touchPosition.x,touchPosition.y);
                break;

            case MouseState.Move:
                //Debug.Log("Move touch detected at position " + Camera.main.ScreenToWorldPoint(Input.mousePosition));
                touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                break;

            case MouseState.Ended:
                //Debug.Log("End touch detected at position " + Camera.main.ScreenToWorldPoint(Input.mousePosition));
                touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                touchEnd = new Vector2(touchPosition.x, touchPosition.y);
                CalculateDistance(touchBegan, touchEnd);
                touchEnd = new Vector2(0, 0);
                touchPosition = originTouch;
                distance = 0;
                //Debug.Log("Began = "+touchBegan+" End = "+touchEnd+"distance = "+distance);
                break;

            case MouseState.Null:
                //Debug.Log("No Input");
                break;
        }

    }
    float CalculateDistance(Vector2 began,Vector2 end)
    {
        distance = Vector2.Distance(began, end);
        //Debug.Log("Calculated. Began = " + touchBegan + " End = " + touchEnd + "distance = " + distance);
        return distance;
    }
}
