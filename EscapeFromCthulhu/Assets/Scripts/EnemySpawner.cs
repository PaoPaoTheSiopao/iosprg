﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    EnemyPool enemPooler;

    public List<Transform> spawnPoints;
    public List<EnemySet> waves;
    //public List<GameObject> currentWave;
    

    // Start is called before the first frame update
    void Start()
    {
        enemPooler = EnemyPool.Instance;
        StartCoroutine(SpawnWave());
    }

    // Update is called once per frame
    void Update()
    {
        /*
            foreach (EnemySet enem in waves)
            {
                foreach (EnemyType ty in enem.set)
                {
                    SpawnTheEnemies(ty.ToString());
                }
            }
       */
    }

    public void SpawnTheEnemies(string tag)
    {
        int spwPnt = Random.Range(0, spawnPoints.Count);
        float randPos = Random.Range(-10, 10);
        enemPooler.SpawnEnemyFromPool(tag, new Vector3(spawnPoints[spwPnt].transform.position.x + randPos, spawnPoints[spwPnt].transform.position.y, this.transform.position.z), Quaternion.identity);
        //currentWave.Add(enemPooler.SpawnEnemyFromPool(tag, new Vector3(spawnPoints[spwPnt].transform.position.x + randPos, spawnPoints[spwPnt].transform.position.y, this.transform.position.z), Quaternion.identity));
    }

    IEnumerator SpawnWave()
    {
        int i = Random.Range(0, Datas.Instance.currentWaves);
        EnemySet wav = waves[i];
        //Debug.Log("name = " + wav.name);
        foreach (EnemyType typ in wav.set)
        {
            //print(typ.ToString());
            SpawnTheEnemies(typ.ToString());
        }
        //yield return new WaitUntil(() => currentWave.Count == 0);
        yield return new WaitForSeconds(3);

        StartCoroutine(SpawnWave());
    }
}
